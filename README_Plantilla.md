# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

Para comenzar esta práctica es necesario tener instalado lo siguiente:

CODE:

	$ sudo snap install --classic code

POSTMAN: 

	$ sudo snap install postman

EL GESTOR DE PAQUETES NODE (NPM):

	$ sudo apt update
	$ sudo apt install npm 

UNA UTILIDAD QUE AYUDA A INSTALAR Y MANTENER LAS VERSIONES NODE:

	$ sudo npm i -g n

VERSIÖN ESTABLE DE NODE JS:
	$ sudo n stable

GESTOR DE REPOSITORIOS (GIT):

	$ sudo apt install git
	$ git config --global user.name lhc15
	$ git config --global user.mail lhc15@alu.ua.es
	$ git config --list

CREAREMOS NUESTRO ARCHIVO DE CÓDIGO:

index.js en visual code studio

EXPRESS:

	$ npm i -S express


### Instalación 🔧



Instalaremos MongoDB, para ello haremos los siguientes comandos en la terminal

$ sudo apt update
$ sudo apt install -y mongodb


Instalaremos también las bibliotecas mongodb y mongojs dentro de nuestra carpeta node:

	Nos aseguramos que estamos dentro de nuestra carpeta carpeta node, si no es así pondremos los comandos:

	cd node
	cd api-rest


Una vez dentro intalaremos las bibliotecas con los siguientes comandos:

	npm i -S mongodb
	npm i -S mongojs


Para poner en marcha la base de datos usaremos systemctl:

	$ sudo systemctl start mongodb 

De este modo estará inicializada siempre. En caso de querer pararlo usaremos stop:

	$ sudo systemctl stop mongodb

Si al iniciar nuestra máquina no sabemos el estado podremos comprobarlo mediante show:

	$ sudo systemctl show mongodb

Para verificar su funcionamiento también podemos introducir:

	$ mongo --eval 'db.runCommand({ connectionStatus: 1})'

Para crear nuestro servicio API RESTFUL abriremos nuestro editor y editaremos nuestro index.js

Nos aseguramos como antes de que estamos dentro de nuestra carpeta api-rest y ponemos el comando:

	$ code .

El archivo debe quedar tal que así para que funcione:

	'use strict'
	
	const port = process.env.PORT || 3000
	
	const express = require('express');
	const logger = require('morgan');
	const mongojs = require('mongojs');
	
	const app = express();
	
	var db = mongojs("SD");
	var id = mongojs.ObjectID;
	
	app.use(logger('dev'));
	app.use(express.urlencoded({extended: false})) 
	app.use(express.json())
	
	app.param("coleccion", (req, res, next, coleccion) => {
		console.log('param /api/:coleccion');
		console.log('colección: ', coleccion);
	
		req.collection = db.collection(coleccion);
		return next();
	});
	
	app.get('/api', (req, res, next) => {
		console.log('GET /api');
		console.log(req.params);
		console.log(req.collection);
	
		db.getCollectionNames((err, colecciones) => {
			if (err) return next(err);
			res.json(colecciones);
		});	
	});
	
	
	app.get('/api/:coleccion', (req, res, next) => {
		req.collection.find((err, coleccion) => {
			if (err) return next(err);
			res.json(coleccion);
		});
	});
	
			
	app.get('/api/:coleccion/:id', (req, res, next) => {
		req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
			if (err) return next(err);
			res.json(elemento);
		});
	});
	
	
	app.post('/api/:coleccion', (req, res, next) => {
		const elemento = req.body;
	
		if (!elemento.nombre) {
			res.status(400).json ({
				error: 'Bad data',
				description: 'Se precisa al menos un campo <nombre>'
		    });
		} else {
			req.collection.save(elemento, (err, coleccionGuardada) => {
				if(err) return next(err);
				res.json(coleccionGuardada);
			});
		}
	});
	
	
	app.put('/api/:coleccion/:id', (req, res, next) => {
		let elementoId = req.params.id;
		let elementoNuevo = req.body;
		req.collection.update({_id: id(elementoId)},
				{$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
			if (err) return next(err);
			res.json(elementoModif);
		});
	});
	
	app.delete('/api/:coleccion/:id', (req, res, next) => {
		let elementoId = req.params.id;
	
		req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
			if (err) return next(err);
			res.json(resultado);
		});
	});
	
	
	// Iniciamos la aplicación
	app.listen(port, () => {
		console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
	});


## Ejecutando las pruebas ⚙️


Para comprobar que nuestro servidor funciona, vamos a ir creando, probando y guardando rutas API.

En nuestro Postman, introduciremos nuestra cuenta y crearemos un espacio de trabajo, es decir una carpeta donde iremos añadiendo esas rutas. La primera ruta será un POST, donde se creará una colección llamada familia donde el cuerpo estará compuesto por tipo, nombre y edad. 

	POST http://localhost:3000/api/familia

También crearemos otra colección llamada mascotas donde el cuerpo estará formado por tipo, raza y edad.

	POST http://localhost:3000/api/mascotas

Luego crearemos una nueva ruta que será un GET. 
Si la petición es esta 

	GET http://localhost:3000/api/familia

La respuesta será esos datos introducidos más un id creado automáticamente.

Si la petición es 

	GET http://localhost:3000/api/mascotas

La respuesta será también los datos anteriormente introducidos más el id.

Por último crearemos una última ruta PUT. Si ponemos un apetición indicando la colección y el id de algún compenente de esta colección y en el cuerpo añadimos nuevas características como color o indicamos un cambio en elguna ya existente, todos esos cambios se producirán.

La petición con un id de ejemplo sería
	
	PUT http://localhost:3000/api/mascotas/5ad6551ff6efc76d03ddbe70

Si tras esto volvemos al GET y hacemos una petición de esa colección, tendríamos que observar los cambios hechos sin ningún problema.

	GET http://localhost:3000/api/mascotas




### Analice las pruebas end-to-end 🔩

Para verificar lo que hemos hecho nos iremos una terminal de texto, en caso de no tener una abierta presionaremos Ctrl+Alt+T
Una vez en la terminal introduciremos los siguientes comandos:

	$ mongo

Indicaremos que nos muestre la base de datos

	> show dbs

Indicaremos que use SD

	> use SD

Indicaremos que muestre las colecciones, donde nos debería responder el nombre de las colecciones creadas, en este caso familia y mascotas

	> show collections

Indicaremos que nos muestre la información por la que está compuesta cada colección medieante los sigueintes comandos

	> db.familia.find()
	> db.mascotas.find()


### Y las pruebas de estilo de codificación ⌨️

Por último mostraremos un ejemplo de todo lo creado desde la terminal.

Como siempre comprobamos que estemos en la carpeta api-rest. Después pondremos el comando:

	npm start

Esto nos mostrará las rutas POST, GET, PUT de nuestro API REST

En otra terminal pondremos los comandos:

	$ mongo

Esto iniciará mongoDB y nos mostrará su información sobre la versión

	> show dbs 

Nos mostrará la información sobre la base de datos

	> use SD
 
	> show collections
Nos mostrará las colecciones


	> db.coleccion1.find()
	> db.coleccion2.find()
Nos mostrará de lo que está compuesta cada colección

	> db.coleccion1.find().pretty()
Nos mostrará la colección tal cual


## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS
* [POSTMAN]
* [VISUAL CODE]
## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Lorena Heras** - *Documentación* - [lhc15](git clone https://lhc15@bitbucket.org/lhc15/p1.git)


También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
